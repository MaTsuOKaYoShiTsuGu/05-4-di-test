package com.twuc.webApp.domain.mazeGenerator;

class PriorityQueueNode implements Comparable<PriorityQueueNode> {
    private int priority;
    private GridCell currentCell;
    private GridCell previousCell;

    public PriorityQueueNode(int priority, GridCell currentCell, GridCell previousCell) {
        this.priority = priority;
        this.currentCell = currentCell;
        this.previousCell = previousCell;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public GridCell getCurrentCell() {
        return currentCell;
    }

    public void setCurrentCell(GridCell currentCell) {
        this.currentCell = currentCell;
    }

    public GridCell getPreviousCell() {
        return previousCell;
    }

    public void setPreviousCell(GridCell previousCell) {
        this.previousCell = previousCell;
    }

    @Override
    public int compareTo(PriorityQueueNode o) {
        return Integer.compare(this.priority, o.getPriority());
    }
}
